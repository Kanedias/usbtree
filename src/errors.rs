
#[derive(Debug)]
pub(crate) enum Error {
    RusbError(rusb::Error)
}

impl From<rusb::Error> for Error {
    fn from(rusb_error: rusb::Error) -> Self {
        Error::RusbError(rusb_error)
    }
}