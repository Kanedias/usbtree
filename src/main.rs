use ansi_term::{Colour, Style};
use clap::{App, Arg};
use rusb::{Device, GlobalContext};

use std::cell::{Ref, RefCell};
use std::collections::HashMap;
use std::rc::Rc;
use std::vec::Vec;

use core::sync::atomic::AtomicBool;
use core::sync::atomic::Ordering;

mod errors;
mod utils;

#[derive(Debug)]
struct DeviceNode {
    parent: Option<Rc<RefCell<DeviceNode>>>,
    children: Vec<Rc<RefCell<DeviceNode>>>,
    device: Device<GlobalContext>,
}

struct SuppressedError {
    device: Rc<RefCell<DeviceNode>>,
    error: errors::Error,
}

impl DeviceNode {
    fn new(device: Device<GlobalContext>) -> Self {
        DeviceNode {
            parent: None,
            children: vec![],
            device,
        }
    }
}

static IS_VERBOSE: AtomicBool = AtomicBool::new(false);
static IS_COLORED: AtomicBool = AtomicBool::new(false);

fn main() {
    let cli_args = App::new("USB device tree enumeration utility")
        .version("0.1.0")
        .author("Oleg `Kanedias` Chernovskiy <kanedias@house-of-maker.online>")
        .about("Shows device tree with proper names and details")
        .arg(
            Arg::with_name("verbose")
                .short("v")
                .long("verbose")
                .required(false)
                .help("Verbose output"),
        )
        .arg(
            Arg::with_name("color")
                .short("c")
                .long("color")
                .required(false)
                .help("Colored output"),
        )
        .after_help("Note: if you get unknown devices/serials, try running usbtree as root")
        .get_matches();

    IS_VERBOSE.store(cli_args.is_present("verbose"), Ordering::Relaxed);
    IS_COLORED.store(cli_args.is_present("color"), Ordering::Relaxed);

    // rusb::set_log_level(rusb::LogLevel::Debug);

    let devices = rusb::devices().expect("Couldn't get device list from libusb");

    // create mapping id -> device
    let mut device_table = HashMap::new();
    for device in devices.iter() {
        let node = DeviceNode::new(device);
        device_table.insert(node.device.as_raw() as u64, Rc::new(RefCell::new(node)));
    }

    // assign parent/children relationship
    for device in devices.iter() {
        if let Some(parent) = device.get_parent() {
            let parent_node = device_table[&(parent.as_raw() as u64)].clone();
            let device_node = device_table[&(device.as_raw() as u64)].clone();

            device_node.borrow_mut().parent = Some(parent_node.clone());
            parent_node.borrow_mut().children.push(device_node);

            // sort so that ports are in natural order
            parent_node
                .borrow_mut()
                .children
                .sort_by_key(|node| node.borrow().device.port_number());
        }
    }

    let roots: Vec<_> = device_table
        .values()
        .filter(|dev| dev.borrow().parent.is_none())
        .collect();

    if roots.is_empty() {
        println!("No USB devices found on the system");
        return;
    }

    println!("╤");
    let mut suppressed: Vec<SuppressedError> = vec![];
    for (idx, root_node) in roots.iter().enumerate() {
        report_device(root_node, "", idx == roots.len() - 1, &mut suppressed);
    }

    if !IS_VERBOSE.load(Ordering::Relaxed) {
        // no error reporting
        return;
    }

    println!();
    for item in suppressed {
        let dev = item.device.borrow();
        let dev_desc = dev.device.device_descriptor().expect("Descriptor open must succeed");

        match item.error {
            errors::Error::RusbError(rusb_err) => {
                eprintln!(
                    "Warning: Device {:04x}:{:04x}, {}",
                    dev_desc.vendor_id(),
                    dev_desc.product_id(),
                    rusb_err
                );
            }
        }
    }
}

fn report_device(device_node: &Rc<RefCell<DeviceNode>>, indent: &str, last: bool, errors: &mut Vec<SuppressedError>) {
    let device_node_ref: Ref<DeviceNode> = device_node.borrow();

    // branch that leads to current item
    let mut current_indent = String::from(indent);

    // branches that lead to children of current item
    let mut child_indent = String::from(indent);

    match last {
        false => {
            current_indent.push_str("├── ");
            child_indent.push_str("│   ");
        }
        true => {
            current_indent.push_str("└── ");
            child_indent.push_str("    ");
        }
    }

    // branches that are used for filling up space after current item
    let mut extended_indent = child_indent.clone();
    if !device_node_ref.children.is_empty() {
        extended_indent.push('│');
    }

    // since libusb 1.0.16, this function always succeeds
    let dev_desc = device_node_ref
        .device
        .device_descriptor()
        .expect("Descriptor open must succeed");
    let dev_handle = device_node_ref.device.open();

    let manufacturer_name: String;
    let product_name: String;
    let product_serial: String;
    if let Err(error_desc) = dev_handle {
        // couldn't open device, probably not available to user?
        errors.push(SuppressedError {
            device: device_node.clone(),
            error: error_desc.into(),
        });

        // try to get info from cached ids
        let cached = usb_ids::Device::from_vid_pid(dev_desc.vendor_id(), dev_desc.product_id());
        if let Some(known_device) = cached {
            // we have the device in the known database
            manufacturer_name = known_device.vendor().name().to_owned();
            product_name = known_device.name().to_owned();
        } else {
            // not found in cached ids
            manufacturer_name = format!("Unknown vendor {:04x}", dev_desc.vendor_id());
            product_name = format!("Unknown product {:04x}", dev_desc.product_id());
        }

        // no chance to get the serial without the handle
        product_serial = "Unknown".to_string();
    } else {
        let device_handle = dev_handle.unwrap();
        manufacturer_name = device_handle.read_manufacturer_string_ascii(&dev_desc).unwrap_or_else(|_| "N/A".to_string());
        product_name = device_handle.read_product_string_ascii(&dev_desc).unwrap_or_else(|_| "N/A".to_string());
        product_serial = device_handle.read_serial_number_string_ascii(&dev_desc).unwrap_or_else(|_| "N/A".to_string());
    }

    let mut accent_style = Style::new();
    let mut device_style = Style::new();
    let mut details_style = Style::new();

    if IS_COLORED.load(Ordering::Relaxed) {
        accent_style = accent_style.bold().fg(Colour::Fixed(255));
        device_style = device_style.bold().fg(Colour::Green);
        details_style = device_style.dimmed().fg(Colour::Fixed(245));
    }

    let device_line = format!("{}: {}", manufacturer_name, product_name);
    let mut device_prefix = String::new();
    let mut details_line = String::new();

    let port_number = device_node_ref.device.port_number();
    if port_number > 0 {
        device_prefix = format!("Port {}: ", port_number);
    }

    if IS_VERBOSE.load(Ordering::Relaxed) {
        let dev_bus = device_node_ref.device.bus_number();
        let dev_speed = device_node_ref.device.speed();
        let dev_addr = device_node_ref.device.address();

        details_line = format!(
            "Bus: {:03}, Address: {:03}\nUSB: {}, Speed: {}\nClass: {}, Serial: {}",
            dev_bus,
            dev_addr,
            dev_desc.usb_version(),
            utils::from_speed(dev_speed),
            utils::from_class_code(dev_desc.class_code()),
            product_serial
        );
    }

    println!("{}{}{}", current_indent, accent_style.paint(device_prefix), device_style.paint(device_line));
    if !details_line.is_empty() {
        for line in details_line.split('\n') {
            println!("{}{}", child_indent, details_style.paint(line));
        }

        // put one empty line after the details to better separate the items
        println!("{}", extended_indent.trim_end());
    }

    // process nested devices
    for (idx, nested) in device_node_ref.children.iter().enumerate() {
        report_device(&nested, &child_indent,idx == device_node_ref.children.len() - 1, errors);
    }
}
