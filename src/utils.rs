
pub(crate) fn from_speed(speed: rusb::Speed) -> &'static str {
    match speed {
        rusb::Speed::Unknown => "Unknown",
        rusb::Speed::Low => "1.5 Mbps",
        rusb::Speed::Full => "12 Mbps",
        rusb::Speed::High => "480 Mbps",
        rusb::Speed::Super => "5 Gbps",
    }
}

pub(crate) fn from_class_code(class_code: u8) -> &'static str {
    match class_code {
        rusb::constants::LIBUSB_CLASS_PER_INTERFACE => "Interface-specific",
        rusb::constants::LIBUSB_CLASS_AUDIO => "Audio",
        rusb::constants::LIBUSB_CLASS_COMM => "Communications",
        rusb::constants::LIBUSB_CLASS_HID => "Human Interface",
        rusb::constants::LIBUSB_CLASS_PHYSICAL => "Physical",
        rusb::constants::LIBUSB_CLASS_PRINTER => "Printer",
        rusb::constants::LIBUSB_CLASS_IMAGE => "Image",
        rusb::constants::LIBUSB_CLASS_MASS_STORAGE => "Mass storage",
        rusb::constants::LIBUSB_CLASS_HUB => "Hub",
        rusb::constants::LIBUSB_CLASS_DATA => "Data",
        rusb::constants::LIBUSB_CLASS_SMART_CARD => "Smartcard",
        rusb::constants::LIBUSB_CLASS_CONTENT_SECURITY => "Content security",
        rusb::constants::LIBUSB_CLASS_VIDEO => "Video",
        rusb::constants::LIBUSB_CLASS_PERSONAL_HEALTHCARE => "Personal healthcare",
        rusb::constants::LIBUSB_CLASS_DIAGNOSTIC_DEVICE => "Diagnostic device",
        rusb::constants::LIBUSB_CLASS_WIRELESS => "Wireless",
        rusb::constants::LIBUSB_CLASS_APPLICATION => "Application",
        rusb::constants::LIBUSB_CLASS_VENDOR_SPEC => "Vendor-specific",
        _ => "Unknown"
    }
}