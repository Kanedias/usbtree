[![build status](https://gitlab.com/Kanedias/usbtree/badges/master/pipeline.svg)](https://gitlab.com/Kanedias/usbtree/commits/master)
[![License](https://img.shields.io/badge/License-GPLv3%2B-brightgreen)](https://www.gnu.org/licenses/gpl-3.0.html)
[![Universal binary](https://img.shields.io/badge/Universal-ELF-brightgreen.svg)](https://gitlab.com/Kanedias/usbtree/-/jobs/artifacts/master/browse?job=release+static)

USBTREE
=======

A simple binary to show the tree of available USB devices on the system.
which is more readable than what `lsusb -vt` shows and provides better overall info.

Disclaimer: I only tested this on GNU/Linux systems, but this may as well work on Windows
if you do `vcpkg install libusb` first.

Installation
------------

If you're familiar with Rust you can just execute `cargo install usbtree` and get it in `~/.cargo/bin`,
or run `cargo build --release` after cloning this repo to a folder.

Otherwise you can download [latest static build](https://gitlab.com/Kanedias/usbtree/-/jobs/artifacts/master/browse?job=release+static).
This is a binary that will most likely work on any GNU/Linux distribution.

If it doesn't, or in case you need this for specific cases like Android or RPi devices, create an issue here.

Example output
--------------

<details><summary>Simple</summary>

```
╤
├── Linux 4.15.0-147-generic xhci-hcd: xHCI Host Controller
│   ├── VIA Labs, Inc.: USB2.0 Hub
│   │   ├── VIA Labs, Inc.: USB2.0 Hub
│   │   │   └── Google: AOSP on blueline
│   │   ├── SAMSUNG: SAMSUNG_Android
│   │   ├── VIA Labs, Inc.: USB2.0 Hub
│   │   │   ├── SAMSUNG: SAMSUNG_Android
│   │   │   └── SAMSUNG: SAMSUNG_Android
│   │   └── VIA Labs, Inc.: USB2.0 Hub
│   │       ├── SAMSUNG: SAMSUNG_Android
│   │       ├── HUAWEI: BAH3-W09
│   │       └── Sony: H9436
│   └── VIA Labs, Inc.: USB2.0 Hub
│       ├── VIA Labs, Inc.: USB2.0 Hub
│       │   ├── HUAWEI: HMA-L29
│       │   ├── HUAWEI: ELE-L29
│       │   ├── HUAWEI: STK-LX1
│       ├── HUAWEI: EBG-AN10
│       ├── VIA Labs, Inc.: USB2.0 Hub
│       │   ├── HUAWEI: DNN-LX9
│       │   ├── HUAWEI: ANA-NX9
│       │   ├── HUAWEI: LIO-AL00
│       │   └── HUAWEI: ELS-NX9
│       └── VIA Labs, Inc.: USB2.0 Hub
│           ├── Unisoc: Unisoc Phone
│           ├── Xiaomi: ATOLL-AB-IDP _SN:9CB93DDE
│           └── Xiaomi: Redmi 9A
└── Linux 4.15.0-147-generic xhci-hcd: xHCI Host Controller
    └── VIA Labs, Inc.: USB3.0 Hub
        ├── VIA Labs, Inc.: USB3.0 Hub
        ├── VIA Labs, Inc.: USB3.0 Hub
        └── VIA Labs, Inc.: USB3.0 Hub
```

</details>

<details><summary>Verbose (with sudo)</summary>

```
╤
├── Linux 5.12.11-arch1-1 xhci-hcd: xHCI Host Controller
│   Bus: 002, Address: 001
│   USB: 3.1.0, Speed: Unknown
│   Class: Hub, Serial: 0000:07:00.1
│
├── Linux 5.12.11-arch1-1 xhci-hcd: xHCI Host Controller
│   Bus: 003, Address: 001
│   USB: 2.0.0, Speed: 480 Mbps
│   Class: Hub, Serial: 0000:07:00.3
│   │
│   └── Port 2: Linux 5.13.0-postmarketos-qcom-msm8974 with ci_hdrc_msm: RNDIS/Ethernet Gadget
│       Bus: 003, Address: 010
│       USB: 2.0.0, Speed: 480 Mbps
│       Class: Communications, Serial: N/A
│
├── Linux 5.12.11-arch1-1 xhci-hcd: xHCI Host Controller
│   Bus: 006, Address: 001
│   USB: 3.1.0, Speed: Unknown
│   Class: Hub, Serial: 0000:0e:00.3
│
├── Linux 5.12.11-arch1-1 xhci-hcd: xHCI Host Controller
│   Bus: 001, Address: 001
│   USB: 2.0.0, Speed: 480 Mbps
│   Class: Hub, Serial: 0000:07:00.1
│   │
│   ├── Port 1: Logitech: G502 HERO Gaming Mouse
│   │   Bus: 001, Address: 002
│   │   USB: 2.0.0, Speed: 12 Mbps
│   │   Class: Interface-specific, Serial: 097D39693034
│   │
│   └── Port 2: Logitech: PRO Gaming Keyboard
│       Bus: 001, Address: 003
│       USB: 2.0.0, Speed: 12 Mbps
│       Class: Interface-specific, Serial: 0C8539413532
│
├── Linux 5.12.11-arch1-1 xhci-hcd: xHCI Host Controller
│   Bus: 004, Address: 001
│   USB: 3.1.0, Speed: Unknown
│   Class: Hub, Serial: 0000:07:00.3
│
└── Linux 5.12.11-arch1-1 xhci-hcd: xHCI Host Controller
    Bus: 005, Address: 001
    USB: 2.0.0, Speed: 480 Mbps
    Class: Hub, Serial: 0000:0e:00.3
    │
    ├── Port 3: Sunplus IT Co : ZET USB WEBCAM
    │   Bus: 005, Address: 002
    │   USB: 2.0.0, Speed: 480 Mbps
    │   Class: Unknown, Serial: 20190313001
    │
    └── Port 4: N/A: CSR8510 A10
        Bus: 005, Address: 003
        USB: 2.0.0, Speed: 12 Mbps
        Class: Wireless, Serial: N/A
```

</details>
<br/>

Contributions
-------------

You may create merge request or bug/enhancement issue right here on GitLab, or send formatted patch via e-mail.
For details see CONTRIBUTING.md file in this repo.

License
-------------

    Copyright (C) 2021  Oleg `Kanedias` Chernovskiy

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

